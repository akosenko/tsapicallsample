package ru.aak.demo.listener;

import javax.telephony.CallEvent;
import javax.telephony.ConnectionEvent;
import javax.telephony.MetaEvent;
import javax.telephony.TerminalConnectionEvent;
import javax.telephony.callcontrol.CallControlConnectionEvent;
import javax.telephony.callcontrol.CallControlTerminalConnectionEvent;
import javax.telephony.callcontrol.CallControlTerminalConnectionListener;

public class ExampleCallControlTerminalConnectionListener implements CallControlTerminalConnectionListener {



  private void log(String name, int eventCause) {
    System.out.println("[ExampleCallControlTerminalConnectionListener] " + name + "; cause: " + eventCause);
  }

  private void log(String... names) {
    System.out.println("[ExampleCallControlTerminalConnectionListener] " + String.join(", ", names));
  }

  @Override
  public void terminalConnectionBridged(CallControlTerminalConnectionEvent event) {
    log("terminalConnectionBridged", event.getCause());
  }

  @Override
  public void terminalConnectionDropped(CallControlTerminalConnectionEvent event) {
    log("terminalConnectionDropped", event.getCause());
  }

  @Override
  public void terminalConnectionHeld(CallControlTerminalConnectionEvent event) {
    log("terminalConnectionHeld", event.getCause());
  }

  @Override
  public void terminalConnectionInUse(CallControlTerminalConnectionEvent event) {
    log("terminalConnectionInUse", event.getCause());
  }

  @Override
  public void terminalConnectionRinging(CallControlTerminalConnectionEvent event) {
    log("terminalConnectionRinging", event.getCause());
  }

  @Override
  public void terminalConnectionTalking(CallControlTerminalConnectionEvent event) {
    log("terminalConnectionTalking", event.getCause());
  }

  @Override
  public void terminalConnectionUnknown(CallControlTerminalConnectionEvent event) {
    log("terminalConnectionUnknown", event.getCause());
  }

  @Override
  public void terminalConnectionActive(TerminalConnectionEvent event) {
    log("terminalConnectionActive", event.getCause());
  }

  @Override
  public void terminalConnectionCreated(TerminalConnectionEvent event) {
    log("terminalConnectionCreated", event.getCause());
  }

  @Override
  public void terminalConnectionDropped(TerminalConnectionEvent event) {
    log("terminalConnectionDropped", event.getCause());
  }

  @Override
  public void terminalConnectionPassive(TerminalConnectionEvent event) {
    log("terminalConnectionPassive", event.getCause());
  }

  @Override
  public void terminalConnectionRinging(TerminalConnectionEvent event) {
    log("terminalConnectionRinging", event.getCause());
  }

  @Override
  public void terminalConnectionUnknown(TerminalConnectionEvent event) {
    log("terminalConnectionUnknown", event.getCause());
  }

  @Override
  public void connectionAlerting(CallControlConnectionEvent event) {
    log("connectionAlerting", event.getCause());
    log("connectionAlerting", "getCalledAddress", event.getCalledAddress().getName());
    log("connectionAlerting", "getCallingAddress", event.getCallingAddress().getName());
  }

  @Override
  public void connectionDialing(CallControlConnectionEvent event) {
    log("connectionDialing", event.getCause());
  }

  @Override
  public void connectionDisconnected(CallControlConnectionEvent event) {
    log("connectionDisconnected", event.getCause());
  }

  @Override
  public void connectionEstablished(CallControlConnectionEvent event) {
    log("connectionEstablished", event.getCause());
  }

  @Override
  public void connectionFailed(CallControlConnectionEvent event) {
    log("connectionFailed", event.getCause());
  }

  @Override
  public void connectionInitiated(CallControlConnectionEvent event) {
    log("connectionInitiated", event.getCause());
  }

  @Override
  public void connectionNetworkAlerting(CallControlConnectionEvent event) {
    log("connectionNetworkAlerting", event.getCause());
    log("connectionNetworkAlerting", "getCalledAddress", event.getCalledAddress().getName());
    log("connectionNetworkAlerting", "getCallingAddress", event.getCallingAddress().getName());
  }

  @Override
  public void connectionNetworkReached(CallControlConnectionEvent event) {
    log("connectionNetworkReached", event.getCause());
  }

  @Override
  public void connectionOffered(CallControlConnectionEvent event) {
    log("connectionOffered", event.getCause());
  }

  @Override
  public void connectionQueued(CallControlConnectionEvent event) {
    log("connectionQueued", event.getCause());
  }

  @Override
  public void connectionUnknown(CallControlConnectionEvent event) {
    log("connectionUnknown", event.getCause());
  }

  @Override
  public void connectionAlerting(ConnectionEvent event) {
    log("connectionAlerting", event.getCause());
  }

  @Override
  public void connectionConnected(ConnectionEvent event) {
    log("connectionConnected", event.getCause());
  }

  @Override
  public void connectionCreated(ConnectionEvent event) {
    log("connectionCreated", event.getCause());
  }

  @Override
  public void connectionDisconnected(ConnectionEvent event) {
    log("connectionDisconnected", event.getCause());
  }

  @Override
  public void connectionFailed(ConnectionEvent event) {
    log("connectionFailed", event.getCause());
  }

  @Override
  public void connectionInProgress(ConnectionEvent event) {
    log("connectionInProgress", event.getCause());
  }

  @Override
  public void connectionUnknown(ConnectionEvent event) {
    log("connectionUnknown", event.getCause());
  }

  @Override
  public void callActive(CallEvent event) {
    log("callActive", event.getCause());
  }

  @Override
  public void callInvalid(CallEvent event) {
    log("callInvalid", event.getCause());
  }

  @Override
  public void callEventTransmissionEnded(CallEvent event) {
    log("callEventTransmissionEnded", event.getCause());
  }

  @Override
  public void singleCallMetaProgressStarted(MetaEvent event) {
    log("singleCallMetaProgressStarted", event.getCause());
  }

  @Override
  public void singleCallMetaProgressEnded(MetaEvent event) {
    log("singleCallMetaProgressEnded", event.getCause());
  }

  @Override
  public void singleCallMetaSnapshotStarted(MetaEvent event) {
    log("singleCallMetaSnapshotStarted", event.getCause());
  }

  @Override
  public void singleCallMetaSnapshotEnded(MetaEvent event) {
    log("singleCallMetaSnapshotEnded", event.getCause());
  }

  @Override
  public void multiCallMetaMergeStarted(MetaEvent event) {
    log("multiCallMetaMergeStarted", event.getCause());
  }

  @Override
  public void multiCallMetaMergeEnded(MetaEvent event) {
    log("multiCallMetaMergeEnded", event.getCause());
  }

  @Override
  public void multiCallMetaTransferStarted(MetaEvent event) {
    log("multiCallMetaTransferStarted", event.getCause());
  }

  @Override
  public void multiCallMetaTransferEnded(MetaEvent event) {
    log("multiCallMetaTransferEnded", event.getCause());
  }
}

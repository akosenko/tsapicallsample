package ru.aak.demo.listener;

import javax.telephony.CallEvent;
import javax.telephony.CallListener;
import javax.telephony.MetaEvent;

public class ExampleCallListener implements CallListener {

  private void log(String name, int eventCause) {
    System.out.println("[ExampleCallListener] " + name + "; cause: " + eventCause);
  }

  @Override
  public void callActive(CallEvent callEvent) {
    log("callActive", callEvent.getCause());
  }

  @Override
  public void callInvalid(CallEvent callEvent) {
    log("callInvalid", callEvent.getCause());
  }

  @Override
  public void callEventTransmissionEnded(CallEvent callEvent) {
    log("callEventTransmissionEnded", callEvent.getCause());
  }

  @Override
  public void singleCallMetaProgressStarted(MetaEvent metaEvent) {
    log("singleCallMetaProgressStarted", metaEvent.getCause());
  }

  @Override
  public void singleCallMetaProgressEnded(MetaEvent metaEvent) {
    log("singleCallMetaProgressEnded", metaEvent.getCause());
  }

  @Override
  public void singleCallMetaSnapshotStarted(MetaEvent metaEvent) {
    log("singleCallMetaSnapshotStarted", metaEvent.getCause());
  }

  @Override
  public void singleCallMetaSnapshotEnded(MetaEvent metaEvent) {
    log("singleCallMetaSnapshotEnded", metaEvent.getCause());
  }

  @Override
  public void multiCallMetaMergeStarted(MetaEvent metaEvent) {
    log("multiCallMetaMergeStarted", metaEvent.getCause());
  }

  @Override
  public void multiCallMetaMergeEnded(MetaEvent metaEvent) {
    log("multiCallMetaMergeEnded", metaEvent.getCause());
  }

  @Override
  public void multiCallMetaTransferStarted(MetaEvent metaEvent) {
    log("multiCallMetaTransferStarted", metaEvent.getCause());
  }

  @Override
  public void multiCallMetaTransferEnded(MetaEvent metaEvent) {
    log("multiCallMetaTransferEnded", metaEvent.getCause());
  }
}

package ru.aak.demo;

import com.avaya.jtapi.tsapi.adapters.TerminalListenerAdapter;
import com.avaya.jtapi.tsapi.impl.TsapiCall;
import ru.aak.demo.listener.ExampleCallControlTerminalConnectionListener;
import ru.aak.demo.listener.ExampleCallListener;
import ru.aak.demo.listener.ExampleProviderListener;
import ru.aak.demo.listener.ExampleTerminalConnectionListener;

import javax.telephony.*;

import static java.lang.System.exit;

public class TsapiCallSample {

  private static void log(String message) {
    System.out.println("[TsapiCallSample] " + message);
  }

  public static void main(String[] args) {

    /* Ожидаем два аргумента (номер кто звонит и номер куда звоним)*/
    if (args.length < 2) {
      log("Не указаны входящие агрументы");
      exit(1);
    }

    /* Входные параметры */
    String extension = args[0];
    String calledNumber = args[1];

    /* Парамтеры подключения */
    String tlink = "AVAYA#CM1#CSTA#AES-TEST-CC";
    String user = "crmuser";
    String password = "cRmUser0!";
    String providerString = tlink + ";login=" + user + ";passwd=" + password;
    String jtapiPeerName = "com.avaya.jtapi.tsapi.TsapiPeer";

    log("Инициализируем подключение к AES");

    /* Инициализируем подключение к AES */
    JtapiPeer jtapiPeer = null;
    try {
      jtapiPeer = JtapiPeerFactory.getJtapiPeer(jtapiPeerName);
    } catch (JtapiPeerUnavailableException e) {
      log(e.getMessage());
      exit(2);
    }
    Provider provider = jtapiPeer.getProvider(providerString);

    /* Добавляем тестовый листенер */
    ExampleProviderListener exampleProviderListener = new ExampleProviderListener();
    try {
      provider.addProviderListener(exampleProviderListener);
    } catch (ResourceUnavailableException | MethodNotSupportedException e) {
      log(e.getMessage());
      provider.shutdown();
      exit(3);
    }

    /* Тупо подождем пока провайдер подключился */
    int i = 0;
    while (i < 7) {
      if (provider.getState() == Provider.IN_SERVICE) {
        break;
      }
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        log(e.getMessage());
      }
      i++;
    }

    if (provider.getState() != Provider.IN_SERVICE) {
      log("Провайдер не подключен");
      provider.shutdown();
      exit(4);
    }

    log("Создаем сущности Address, Terminal и Call");

    /* Создаем Address, Terminal и Call */
    Address address = null;
    Terminal terminal = null;
    TsapiCall call = null;

    //ExampleCallListener exampleCallListener = new ExampleCallListener();
    ExampleCallControlTerminalConnectionListener exampleCallControlTerminalConnectionListener = new ExampleCallControlTerminalConnectionListener();
    try {
      address = provider.getAddress(extension);
      address.addCallListener(exampleCallControlTerminalConnectionListener);
      terminal = provider.getTerminal(extension);

      call = (TsapiCall) provider.createCall();

      //call.addCallListener(exampleCallListener);

    } catch (InvalidArgumentException | ResourceUnavailableException | InvalidStateException | PrivilegeViolationException | MethodNotSupportedException e) {
      log(e.getMessage());
      provider.shutdown();
      exit(5);
    }

    log(extension + " звонит => " + calledNumber);

    /* Пытаемся позвонить */
    try {
      call.connect(terminal, address, calledNumber);
    } catch (ResourceUnavailableException | PrivilegeViolationException | InvalidPartyException | InvalidArgumentException | InvalidStateException | MethodNotSupportedException e) {
      log(e.getMessage());
      provider.shutdown();
      exit(6);
    }

    log("Звонок состоялся без ошибок");

    String ucid = call.getUCID();

    log("UCID: " + ucid);

    /* Глупо ждем 7 секунд */
    try {
      Thread.sleep(7000);
    } catch (InterruptedException e) {
      log(e.getMessage());
      provider.shutdown();
      exit(7);
    }

    /* завершаем звонок */
    TerminalConnection terminalConnection = call.getConnections()[0].getTerminalConnections()[0];
    try {
      terminalConnection.getConnection().disconnect();
    } catch (PrivilegeViolationException | ResourceUnavailableException | MethodNotSupportedException | InvalidStateException e) {
      log(e.getMessage());
      provider.shutdown();
      exit(8);
    }

    /* Завершаем приложение */
    log("Отключаемся от AES");

    provider.shutdown();

    /* Глупо ждем секунду чтобы соединение закрылось */
    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      log(e.getMessage());
    }    log("Тест завершен");

    exit(0);
  }

}

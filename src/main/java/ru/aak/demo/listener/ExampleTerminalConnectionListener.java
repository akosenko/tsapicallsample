package ru.aak.demo.listener;

import javax.telephony.*;

public class ExampleTerminalConnectionListener implements TerminalConnectionListener {
  private void log(String name, int eventCause) {
    System.out.println("[ExampleTerminalListener] " + name + "; cause: " + eventCause);
  }


  @Override
  public void terminalConnectionActive(TerminalConnectionEvent terminalConnectionEvent) {
    log("terminalConnectionActive", terminalConnectionEvent.getCause());
  }

  @Override
  public void terminalConnectionCreated(TerminalConnectionEvent terminalConnectionEvent) {
    log("terminalConnectionCreated", terminalConnectionEvent.getCause());
  }

  @Override
  public void terminalConnectionDropped(TerminalConnectionEvent terminalConnectionEvent) {
    log("terminalConnectionDropped", terminalConnectionEvent.getCause());
  }

  @Override
  public void terminalConnectionPassive(TerminalConnectionEvent terminalConnectionEvent) {
    log("terminalConnectionPassive", terminalConnectionEvent.getCause());
  }

  @Override
  public void terminalConnectionRinging(TerminalConnectionEvent terminalConnectionEvent) {
    log("terminalConnectionRinging", terminalConnectionEvent.getCause());
  }

  @Override
  public void terminalConnectionUnknown(TerminalConnectionEvent terminalConnectionEvent) {
    log("terminalConnectionUnknown", terminalConnectionEvent.getCause());
  }

  @Override
  public void connectionAlerting(ConnectionEvent connectionEvent) {
    log("connectionAlerting", connectionEvent.getCause());
  }

  @Override
  public void connectionConnected(ConnectionEvent connectionEvent) {
    log("connectionConnected", connectionEvent.getCause());
  }

  @Override
  public void connectionCreated(ConnectionEvent connectionEvent) {
    log("connectionCreated", connectionEvent.getCause());
  }

  @Override
  public void connectionDisconnected(ConnectionEvent connectionEvent) {
    log("connectionDisconnected", connectionEvent.getCause());
  }

  @Override
  public void connectionFailed(ConnectionEvent connectionEvent) {
    log("connectionFailed", connectionEvent.getCause());
  }

  @Override
  public void connectionInProgress(ConnectionEvent connectionEvent) {
    log("connectionInProgress", connectionEvent.getCause());
  }

  @Override
  public void connectionUnknown(ConnectionEvent connectionEvent) {
    log("connectionUnknown", connectionEvent.getCause());
  }

  @Override
  public void callActive(CallEvent callEvent) {
    log("callActive", callEvent.getCause());
  }

  @Override
  public void callInvalid(CallEvent callEvent) {
    log("callInvalid", callEvent.getCause());
  }

  @Override
  public void callEventTransmissionEnded(CallEvent callEvent) {
    log("callEventTransmissionEnded", callEvent.getCause());
  }

  @Override
  public void singleCallMetaProgressStarted(MetaEvent metaEvent) {
    log("singleCallMetaProgressStarted", metaEvent.getCause());
  }

  @Override
  public void singleCallMetaProgressEnded(MetaEvent metaEvent) {
    log("singleCallMetaProgressEnded", metaEvent.getCause());
  }

  @Override
  public void singleCallMetaSnapshotStarted(MetaEvent metaEvent) {
    log("singleCallMetaSnapshotStarted", metaEvent.getCause());
  }

  @Override
  public void singleCallMetaSnapshotEnded(MetaEvent metaEvent) {
    log("singleCallMetaSnapshotEnded", metaEvent.getCause());
  }

  @Override
  public void multiCallMetaMergeStarted(MetaEvent metaEvent) {
    log("multiCallMetaMergeStarted", metaEvent.getCause());
  }

  @Override
  public void multiCallMetaMergeEnded(MetaEvent metaEvent) {
    log("multiCallMetaMergeEnded", metaEvent.getCause());
  }

  @Override
  public void multiCallMetaTransferStarted(MetaEvent metaEvent) {
    log("multiCallMetaTransferStarted", metaEvent.getCause());
  }

  @Override
  public void multiCallMetaTransferEnded(MetaEvent metaEvent) {
    log("multiCallMetaTransferEnded", metaEvent.getCause());
  }
}

package ru.aak.demo.listener;

import javax.telephony.ProviderEvent;
import javax.telephony.ProviderListener;

public class ExampleProviderListener implements ProviderListener {

  private void log(String name, int eventCause) {
    System.out.println("[ExampleProviderListener] " + name + "; cause: " + eventCause);
  }

  @Override
  public void providerInService(ProviderEvent providerEvent) {
    log("providerInService", providerEvent.getCause());
  }

  @Override
  public void providerEventTransmissionEnded(ProviderEvent providerEvent) {
    log("providerEventTransmissionEnded", providerEvent.getCause());
  }

  @Override
  public void providerOutOfService(ProviderEvent providerEvent) {
    log("providerOutOfService", providerEvent.getCause());
  }

  @Override
  public void providerShutdown(ProviderEvent providerEvent) {
    log("providerShutdown", providerEvent.getCause());
  }
}
